module.exports = ctx => ({
  head: [
    ["link", { rel: "icon", href: "/TMS-Logo.png" }],
    ["link", { rel: "manifest", href: "/manifest.json" }],
    ["meta", { name: "theme-color", content: "#787878" }]
  ],
  dest: "public/",
  title: "TMS Handbook",
  description: "The unofficial TMS Handbook",
  themeConfig: {
    sidebarDepth: 1,
    repo: "https://gitlab.com/pinewood-builders/TMS-Handbook",
    editLinks: true,
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: "green",
      disableThemeIgnore: true,
    },
    algolia: 
       {
          apiKey: "8d22e1eefdbc0248fed9c76b1d115c9f",
          indexName: "pinewood-builders_tms",
        }
      ,

    docsDir: "docs/",
    logo: "/TMS-Logo.png",
    smoothScroll: true,
    nav: [
      {
        text: "Home",
        link: "/",
      },
      {
        text: 'Pinewood',
        items: [{
          text: 'Pinewood Homepage',
          link: 'https://pinewood-builders.com'
        },
          {
            text: 'PBST Handbook',
            link: 'https://pbst.pinewood-builders.com'
          },
          {
            text: 'PET Handbook',
            link: 'https://pet.pinewood-builders.com'
          },
        ]
      },
    ],

      sidebar: [{
        collapsable: true,
        title: 'TMS Handbook',
        children: ['/handbook/'],
      }
      ],
  },
  plugins: [
    [
      "@vuepress/pwa",
      {
        serviceWorker: true,
        updatePopup: true,
      },
    ],
    [
      "seo",
      {
        siteTitle: (_, $site) => $site.title,
        title: $page => $page.title,
        description: $page => $page.frontmatter.description,
        author: (_, $site) => $site.themeConfig.author,
        tags: $page => $page.frontmatter.tags,
        twitterCard: _ => 'summary_large_image',
        type: $page => ['articles', 'posts', 'blog'].some(folder => $page.regularPath.startsWith('/' + folder)) ? 'article' : 'website',
        url: (_, $site, path) => ($site.themeConfig.domain || '') + path,
        image: ($page, $site) => $page.frontmatter.image && (($site.themeConfig.domain && !$page.frontmatter.image.startsWith('http') || '') + $page.frontmatter.image),
        publishedAt: $page => $page.frontmatter.date && new Date($page.frontmatter.date),
        modifiedAt: $page => $page.lastUpdated && new Date($page.lastUpdated),
      },
    ],
  ],
});
